import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import 'chat_theme.dart';

class FullPhoto extends StatelessWidget {
  final String url;
  final ChatTheme theme;

  FullPhoto({Key key, @required this.url, @required this.theme})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        backgroundColor: theme.topBarColor,
        title: new Text(
          'Image Preview',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: new FullPhotoScreen(url: url, bgColor: theme.backgroundDarkColor),
    );
  }
}

class FullPhotoScreen extends StatefulWidget {
  final String url;
  final Color bgColor;

  FullPhotoScreen({Key key, @required this.url, this.bgColor})
      : super(key: key);

  @override
  State createState() => new FullPhotoScreenState(url: url);
}

class FullPhotoScreenState extends State<FullPhotoScreen> {
  final String url;

  FullPhotoScreenState({Key key, @required this.url});

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: PhotoView(
      imageProvider: NetworkImage(url),
      backgroundDecoration: BoxDecoration(
        color: widget.bgColor ?? Colors.black,
      ),
    ));
  }
}
