import 'package:flutter/foundation.dart';

class  ChatConfig {
  final String customerId;
  final bool isLibrary;

  ChatConfig({this.customerId, this.isLibrary});

  bool get isCustomerApp {
    return this.customerId == null;
  }

  bool get isMobileLibrary {
    return !kIsWeb && isLibrary;
  }
}
