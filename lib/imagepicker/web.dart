import 'dart:async';
import 'dart:html' as html;

Future<dynamic> pickImage() async {
  final completer = new Completer<dynamic>();
  final html.InputElement input = html.document.createElement('input');
  input
    ..type = 'file'
    ..accept = 'image/*';
  input.onChange.listen((e) async {
    final List<html.File> files = input.files;
    final reader = new html.FileReader();
    reader.readAsDataUrl(files[0]);
    reader.onError.listen((error) => completer.completeError(error));
    await reader.onLoad.first;
//    if (reader.result is String) {
//      final list = (reader.result as String).codeUnits;
//      Uint8List bytes = Uint8List.fromList(list);
//      completer.complete(bytes);
//    } else {
//      completer.complete(reader.result);
//    }
    completer.complete(reader.result);
  });
  input.click();
  return completer.future;
}
