import 'package:image_picker/image_picker.dart';

Future<dynamic> pickImage() async {
  final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
  return pickedFile.path;
}
