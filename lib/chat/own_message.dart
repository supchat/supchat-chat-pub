import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:supchat_chat/chat/themed_chat_container.dart';

import '../utils/extentions.dart';
import 'message_text.dart';
import 'model/message.dart';

class OwnMessage extends StatelessWidget {
  final Message message;
  final Function onImageClick;

  OwnMessage({this.message, this.onImageClick});

  @override
  Widget build(BuildContext context) {
    var theme = ThemedChatContainer.of(context).theme;
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: Center(
            child: message.type == 0
                ? Container(
                    constraints: const BoxConstraints(maxWidth: 240.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: MessageText(
                              text: message.content,
                              color: theme.messageOwnTextColor,
                          ),
                        ),
                        const SizedBox(width: 24.0),
                        Text(
                          message.time.formatTime(),
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                                  color: theme.messageOwnTextColor,
                                  fontSize: 10)),
                        ),
                      ],
                    ),
                    padding: const EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 10.0),
//                    width: 200.0,
                    decoration: BoxDecoration(
                        color: theme.messageOwnBackgroundColor,
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(4.0),
                            topLeft: Radius.circular(20.0),
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0))),
                  )
                : Container(
                    child: FlatButton(
                      child: Material(
                        child: Stack(
                          children: [
                            Image(
                              image: NetworkImage(message.content),
                              width: 200.0,
                              height: 200.0,
                              fit: BoxFit.cover,
                            ),
                            Container(
                              width: 200.0,
                              height: 30.0,
                              decoration: const BoxDecoration(
                                gradient: const LinearGradient(
                                    colors: [
                                      const Color(0x55000000),
                                      const Color(0x00000000),
                                    ],
                                    begin: const FractionalOffset(0.0, 0.0),
                                    end: const FractionalOffset(0.0, 1.0),
                                    stops: [0.0, 1.0],
                                    tileMode: TileMode.clamp),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    right: 16.0, top: 10.0),
                                child: Text(
                                  message.time.formatTime(),
                                  textAlign: TextAlign.end,
                                  style: GoogleFonts.montserrat(
                                      textStyle: const TextStyle(
                                          color: Colors.white, fontSize: 10)),
                                ),
                              ),
                            ),
                          ],
                        ),
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(4.0),
                            topLeft: Radius.circular(20.0),
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                        clipBehavior: Clip.antiAlias,
                      ),
                      onPressed: () {
                        onImageClick(message.content);
                      },
                      padding: EdgeInsets.all(0),
                    ),
                    margin: EdgeInsets.only(
                      bottom: message.isLast && message.isMy ? 20.0 : 6.0,
                      right: 0.0,
                    ),
                  ),
          ),
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.end,
    );
  }
}
