import '../model/chat_user.dart';

abstract class UserRepository {
  Future<ChatUser> getChatUser();
}
