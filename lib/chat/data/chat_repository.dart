import 'dart:async';

import '../model/chat_info.dart';
import '../model/message.dart';

abstract class ChatRepository {

  Future<dynamic> initialize();

  Future<List<Message>> getChatHistory(String chatId);

  Future<dynamic> markChatSeen(String chatId);

  Stream<List<Message>> onNewMessages(String chatId);

  Future<dynamic> sendMessage(String chatId, Message message);

  Future<String> uploadFile(dynamic data);

  Stream<dynamic> updateOnline(String chatId);

  Stream<ChatInfo> loadChatInfo(String chatId);

  void close();
}
