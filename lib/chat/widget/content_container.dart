import 'package:flutter/material.dart';

class ContentContainer extends StatelessWidget {
  final Widget child;
  final Color color;

  const ContentContainer({Key key, this.child, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        boxShadow: [
          const BoxShadow(
            color: Color.fromRGBO(50, 54, 160, 0.5400000214576721),
            offset: Offset(0, 2),
            blurRadius: 25,
          )
        ],
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
          bottomLeft: Radius.circular(0.0),
          bottomRight: Radius.circular(0.0),
        ),
        color: color,
      ),
      child: child,
    );
  }
}
