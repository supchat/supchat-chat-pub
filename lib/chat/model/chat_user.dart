class ChatUser {
  final String id;
  final int timestamp;
  final String nickname;
  final String photoUrl;
  final String pushToken;

  ChatUser(this.id, this.timestamp,
      this.nickname, this.photoUrl, this.pushToken);

  static ChatUser empty = ChatUser(null, null, null, null, null);
}
