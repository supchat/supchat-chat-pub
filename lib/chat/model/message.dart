import 'package:flutter/foundation.dart';

class Message {
  final String idFrom;
  final String content;
  final int type;
  final DateTime time;
  final bool isMy;
  final bool isLast;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const Message({
    @required this.idFrom,
    @required this.content,
    @required this.type,
    @required this.time,
    this.isMy,
    @required this.isLast,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Message &&
          runtimeType == other.runtimeType &&
          idFrom == other.idFrom &&
          content == other.content &&
          type == other.type &&
          time == other.time &&
          isMy == other.isMy &&
          isLast == other.isLast);

  @override
  int get hashCode =>
      idFrom.hashCode ^
      content.hashCode ^
      type.hashCode ^
      time.hashCode ^
      isMy.hashCode ^
      isLast.hashCode;

  @override
  String toString() {
    return 'Message{' +
        ' idFrom: $idFrom,' +
        ' content: $content,' +
        ' type: $type,' +
        ' time: $time,' +
        ' isMy: $isMy,' +
        ' isLast: $isLast,' +
        '}';
  }

  Message copyWith({
    String idFrom,
    String idTo,
    String content,
    int type,
    DateTime time,
    bool isMy,
    bool isLast,
  }) {
    return new Message(
      idFrom: idFrom ?? this.idFrom,
      content: content ?? this.content,
      type: type ?? this.type,
      time: time ?? this.time,
      isMy: isMy ?? this.isMy,
      isLast: isLast ?? this.isLast,
    );
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    return new Message(
      idFrom: map['idFrom'] as String,
      content: map['content'] as String,
      type: map['type'] as int,
      time: map['time'] as DateTime,
      isMy: map['isMy'] as bool,
      isLast: map['isLast'] as bool,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'idFrom': this.idFrom,
      'content': this.content,
      'type': this.type,
      'time': this.time,
      'isMy': this.isMy,
      'isLast': this.isLast,
    } as Map<String, dynamic>;
  }

//</editor-fold>
}
