class ChatInfo {
  final String name;
  final String id;
  final String pictureUrl;
  final Online online;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const ChatInfo({
    this.name,
    this.id,
    this.pictureUrl,
    this.online,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ChatInfo &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          id == other.id &&
          pictureUrl == other.pictureUrl &&
          online == other.online);

  @override
  int get hashCode => name.hashCode ^ id.hashCode ^ pictureUrl.hashCode ^ online.hashCode;

  @override
  String toString() {
    return 'ChatInfo{' +
        ' name: $name,' +
        ' id: $id,' +
        ' pictureUrl: $pictureUrl,' +
        ' online: $online,' +
        '}';
  }

  ChatInfo copyWith({
    String name,
    String id,
    String pictureUrl,
    Online online,
  }) {
    return new ChatInfo(
      name: name ?? this.name,
      id: id ?? this.id,
      pictureUrl: pictureUrl ?? this.pictureUrl,
      online: online ?? this.online,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'id': this.id,
      'pictureUrl': this.pictureUrl,
      'online': this.online,
    };
  }

  factory ChatInfo.fromMap(Map<String, dynamic> map) {
    return new ChatInfo(
      name: map['name'] as String,
      id: map['id'] as String,
      pictureUrl: map['pictureUrl'] as String,
      online: map['online'] as Online,
    );
  }

//</editor-fold>
}

class Online {
  final String online;
  final DateTime lastUpdate;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const Online({
    this.online,
    this.lastUpdate,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Online &&
          runtimeType == other.runtimeType &&
          online == other.online &&
          lastUpdate == other.lastUpdate);

  @override
  int get hashCode => online.hashCode ^ lastUpdate.hashCode;

  @override
  String toString() {
    return 'Online{' + ' online: $online,' + ' lastUpdate: $lastUpdate,' + '}';
  }

  Online copyWith({
    String online,
    DateTime lastUpdate,
  }) {
    return new Online(
      online: online ?? this.online,
      lastUpdate: lastUpdate ?? this.lastUpdate,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'online': this.online,
      'lastUpdate': this.lastUpdate,
    };
  }

  factory Online.fromMap(Map<String, dynamic> map) {
    return new Online(
      online: map['online'] as String,
      lastUpdate: map['lastUpdate'] as DateTime,
    );
  }

//</editor-fold>
}
