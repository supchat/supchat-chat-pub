import 'dart:async';
import 'dart:convert';

//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:supchat_chat/chat/themed_chat_container.dart';
import 'package:supchat_chat/chat/peer_message.dart';
import 'package:supchat_chat/chat/widget/content_container.dart';

import '../utils/extentions.dart';
import '../chat_config.dart';
import '../chat_theme.dart';
import '../full_photo.dart';
import '../imagepicker/shared.dart';
import 'bloc/chat_bloc.dart';
import 'bloc/chat_event.dart';
import 'bloc/chat_state.dart';
import 'data/chat_repository.dart';
import 'data/user_repository.dart';
import 'message_input.dart';
import 'model/message.dart';
import 'own_message.dart';

class ChatScreen extends StatefulWidget {
  final String chatId;
  final String userId;

  final UserRepository userRepository;
  final ChatRepository chatRepository;
  final ChatConfig chatConfig;

  final ChatTheme theme;

  final Function(BuildContext context) onDetailsClick;

  ChatScreen({
    Key key,
    @required this.chatId,
    @required this.userId,
    @required this.userRepository,
    @required this.chatRepository,
    @required this.chatConfig,
    this.theme = const ChatTheme(),
    this.onDetailsClick,
  }) : super(key: key);

  @override
  State createState() => new ChatScreenState(chatId: chatId);
}

class ChatScreenState extends State<ChatScreen> {
  ChatScreenState({Key key, @required this.chatId});

  String chatId;

  final ScrollController listScrollController = new ScrollController();
  final FocusNode focusNode = new FocusNode();

  ChatBloc _chatBloc;

  @override
  void initState() {
    super.initState();
    focusNode.addListener(onFocusChange);

    _chatBloc = ChatBloc(
        chatId, widget.chatRepository, widget.userRepository, widget.chatConfig)
      ..add(ChatOpened());

    _initImages();
  }

  Future _initImages() async {
    // >> To get paths you need these 2 lines
    final manifestContent =
        await DefaultAssetBundle.of(context).loadString('AssetManifest.json');

    final Map<String, dynamic> manifestMap = json.decode(manifestContent);

    print('assets $manifestMap');
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {}
  }

  @override
  Widget build(BuildContext context) {
    return ThemedChatContainer(
      theme: widget.theme,
      child: Scaffold(
        body: BlocProvider<ChatBloc>(
          create: (BuildContext context) {
            return _chatBloc;
          },
          child: widget.chatConfig.isLibrary
              ? Container(
                  color: widget.theme.backgroundColor,
                  child: Column(
                    children: <Widget>[
                      SafeArea(child: _buildToolBar()),
                      _buildMessagesSection(),
                      _buildInput(context),
                    ],
                  ),
                )
              : Stack(
                  children: <Widget>[
                    _buildTopBar(),
                    Container(
                      color: Colors.transparent,
                      margin: const EdgeInsets.only(top: 80.0),
                      child: ContentContainer(
                        color: widget.theme.backgroundColor,
                        child: Column(
                          children: <Widget>[
                            _buildToolBar(),
                            _buildMessagesSection(),
                            _buildInput(context),
                          ],
                        ),
                      ),
                    ),
                    // Loading
                  ],
                ),
        ),
      ),
    );
  }

  SafeArea _buildTopBar() {
    return SafeArea(
      top: false,
      child: Container(
        width: double.maxFinite,
        height: 120.0,
        color: widget.theme.topBarColor,
        child: SvgPicture.asset(
          'packages/${widget.chatConfig.isLibrary ? 'supchat/chat_module/' : 'supchat_chat/'}assets/chat_bg.svg',
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }

  Future getImage(BuildContext context) async {
    try {
      final imageFile = await pickImage();

      if (imageFile != null) {
        BlocProvider.of<ChatBloc>(context).add(SendImage(imageFile));
      } else {
        print('imageFile == null');
      }
    } catch (e) {
      print('getImage exception $e');
    }
  }

  void onSendMessage(BuildContext context, String content, int type) {
    // type: 0 = text, 1 = image
    if (content.trim() != '') {
      var message = Message(
          idFrom: widget.userId,
          time: DateTime.now(),
          content: content,
          type: type,
          isLast: true,
          isMy: true);
      var sendMessageEvent = SendMessageEvent(message);
      BlocProvider.of<ChatBloc>(context).add(sendMessageEvent);

      if (kIsWeb) {
        focusNode.requestFocus();
      }
      listScrollController.animateTo(0.0,
          duration: Duration(milliseconds: 300), curve: Curves.easeOut);
    } else {
//      Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }

  Widget _buildItem(int index, Message message) {
    final messageWidget = message.isMy
        ? OwnMessage(
            message: message,
            onImageClick: (url) {
              focusNode.unfocus();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FullPhoto(
                    url: url,
                    theme: widget.theme,
                  ),
                ),
              );
            },
          )
        : PeerMessage(
            message: message,
            onImageClick: (url) {
              focusNode.unfocus();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FullPhoto(
                    url: url,
                    theme: widget.theme,
                  ),
                ),
              );
            },
          );

    String dateText = message.headerText(_messages.safeGet(index + 1));

    return (dateText == null)
        ? messageWidget
        : _wrapWithDateHeader(messageWidget, dateText);
  }

  Widget _wrapWithDateHeader(Widget messageWidget, String dateText) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Flexible(
                child: Divider(
                  thickness: 1.0,
                  color: widget.theme.lineColor,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                    vertical: 2.0, horizontal: widget.theme.horizontalPadding),
                margin: const EdgeInsets.symmetric(vertical: 15.0),
                child: Text(
                  dateText,
                  style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                          fontSize: 12,
                          color: widget.theme.dateDividerTextColor)),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16.0),
                    border: Border.all(
                      color: widget.theme.lineColor,
                      width: 1.0,
                    )),
              ),
              Flexible(
                child: Divider(
                  thickness: 1.0,
                  color: widget.theme.lineColor,
                ),
              ),
            ],
          ),
          messageWidget
        ]);
  }

  Future<bool> onBackPress() {
    {
//      Firestore.instance
//          .collection('users')
//          .document(id)
//          .updateData({'chattingWith': null});
      Navigator.pop(context);
    }

    return Future.value(false);
  }

  Widget _buildToolBar() {
    return Builder(
      builder: (BuildContext context) => Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Visibility(
                visible: BlocProvider.of<ChatBloc>(context).isCustomer,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: IconButton(
                    icon: const Icon(Icons.arrow_back_rounded),
                    tooltip: 'Back button',
                    color: widget.theme.primaryColor,
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop();
                      });
                    },
                  ),
                ),
              ),
              BlocBuilder<ChatBloc, ChatState>(
                builder: (BuildContext context, ChatState state) {
                  print('blocbuilder ${state.chatInfo}');
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      widget.chatConfig.isCustomerApp
                          ? Container(
                              height: 12.0,
                              width: 12.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color:
                                    state.chatInfo?.online?.online == 'online'
                                        ? Colors.green
                                        : Colors.grey,
                              ),
                            )
                          : _buildPeerAvatar(
                              state.chatInfo?.pictureUrl,
                              state.chatInfo?.name  ?? "Support",
                              state.chatInfo?.online?.online == 'online',
                            ),
                      Container(
                        width: 150.0,
                        padding: const EdgeInsets.only(
                            top: 24.0, bottom: 24.0, left: 12.0),
                        child: Text(
                          '${state.chatInfo?.name ?? state.chatInfo?.id ?? ''}',
                          maxLines: 1,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  color: widget.theme.textColor,
                                  fontWeight: FontWeight.w500)),
                        ),
                      ),
                    ],
                  );
                },
              ),
              Visibility(
                visible: BlocProvider.of<ChatBloc>(context).isCustomer,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: IconButton(
                    icon: const Icon(Icons.more_horiz_rounded),
                    tooltip: 'Details',
                    color: widget.theme.primaryColor,
                    onPressed: () {
                      setState(() {
                        focusNode.unfocus();
                        widget.onDetailsClick?.call(context);
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
          Divider(
            color: widget.theme.lineColor,
            height: 1.0,
            thickness: 1.0,
          ),
        ],
      ),
    );
  }

  Widget _buildPeerAvatar(String avatarUrl, String name, bool isOnline) {
    var nameSafe = name == null || name.isEmpty ? "?" : name;
    return Stack(
      alignment: AlignmentDirectional.bottomEnd,
      children: [
        avatarUrl != null && avatarUrl.isNotEmpty
            ? Material(
                borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                clipBehavior: Clip.antiAlias,
                child: Image(
                  image: NetworkImage(avatarUrl),
                  width: 42.0,
                  height: 42.0,
                  fit: BoxFit.cover,
                ),
              )
            : Container(
                width: 42.0,
                height: 42.0,
                child: CircleAvatar(
                  backgroundColor: color(nameSafe),
                  child: Center(
                    child: Text(
                      nameSafe[0].toUpperCase(),
                      style: GoogleFonts.montserrat(
                        textStyle: const TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
        Container(
          height: 12.0,
          width: 12.0,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: isOnline ? Colors.green : Colors.grey),
        ),
      ],
    );
  }

  Widget _buildLoading() {
    return Positioned(
      child: Container(
        child: Center(
          child: CircularProgressIndicator(
            valueColor:
                AlwaysStoppedAnimation<Color>(widget.theme.primaryColor),
          ),
        ),
        color: Colors.white.withOpacity(0.8),
      ),
    );
  }

  Widget _buildInput(BuildContext context) {
    return SafeArea(
      top: false,
      child: Builder(
        builder: (context) => MessageInput(
          focusNode: focusNode,
          onSendMessage: onSendMessage,
          onAddAttachment: () {
            getImage(context);
          },
        ),
      ),
    );
  }

  Widget _buildMessagesSection() {
    return BlocListener<ChatBloc, ChatState>(
        listenWhen: (_, state) =>
            state is NewMessageState || state is NewMessagesState,
        listener: (BuildContext context, ChatState state) {
          _messages = (state as MessagesHolder).messages;
          if (state is NewMessageState) {
            _chatMessagesGlobalKey.currentState
                ?.insertItem(0, duration: Duration(milliseconds: 200));
          }
        },
        child: BlocBuilder<ChatBloc, ChatState>(
          buildWhen: (prevState, state) =>
              !(state is NewMessageState) && prevState != state,
          builder: (BuildContext context, state) => Expanded(
            child: Builder(
              builder: (context) {
                if (state is ChatLoaded) {
                  return _buildMessagesList(state.messages);
                } else if (state is ImageLoading) {
                  return Stack(children: <Widget>[
                    _buildMessagesList(state.messages),
                    _buildLoading()
                  ]);
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          widget.theme.primaryColor),
                    ),
                  );
                }
              },
            ),
          ),
        ));
  }

  GlobalKey<AnimatedListState> _chatMessagesGlobalKey =
      GlobalKey<AnimatedListState>();

  List<Message> _messages = [];

  Widget _buildMessagesList(List<Message> messages) {
    _messages = messages;
    return AnimatedList(
      key: _chatMessagesGlobalKey,
      padding: EdgeInsets.symmetric(
        horizontal: widget.theme.horizontalPadding,
        vertical: widget.theme.verticalPadding,
      ),
      itemBuilder: (context, index, animation) {
        return index >= _messages.length
            ? Container()
            : SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(0.1, 0),
                  end: Offset.zero,
                ).animate(animation),
                child: SizeTransition(
                    sizeFactor: CurvedAnimation(
                        parent: animation, curve: const Interval(0.0, 1.0)),
                    axisAlignment: 0.0,
                    child: _buildItem(index, _messages[index])));
      },
      reverse: true,
      initialItemCount: _messages.length,
      controller: listScrollController,
    );
  }

  @override
  void dispose() {
    super.dispose();
    focusNode.dispose();
  }

  Color color(String base) {
    final rest = ((base.hashCode % Colors.primaries.length));
    return Colors.primaries[rest];
  }
}

final DateFormat _dateFormat = DateFormat('dd MMMM');

extension on Message {
  String headerText(Message prevMessage) {
    if (time.day == prevMessage?.time?.day) return null;
    if (time.isToday()) {
      return "Today";
    } else if (time.isYesterday()) {
      return "Yesterday";
    } else {
      return _dateFormat.format(time);
    }
  }
}

extension on List<Message> {
  Message safeGet(int index) {
    if (length <= index || index < 0) return null;
    return this[index];
  }
}
