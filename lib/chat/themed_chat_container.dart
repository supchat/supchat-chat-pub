import 'package:flutter/widgets.dart';

import '../chat_theme.dart';

class ThemedChatContainer extends InheritedWidget {
  final ChatTheme theme;

  ThemedChatContainer({this.theme, Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(ThemedChatContainer old) => theme != old.theme;

  static ThemedChatContainer of(BuildContext context) {
    final ThemedChatContainer result =
        context.dependOnInheritedWidgetOfExactType<ThemedChatContainer>();
    assert(result != null, 'No ThemedChatContainer found in context');
    return result;
  }
}
