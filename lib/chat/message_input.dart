import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:supchat_chat/chat/themed_chat_container.dart';

class MessageInput extends StatefulWidget {
  final Function onSendMessage;
  final Function onAddAttachment;
  final FocusNode focusNode;

  MessageInput({this.onSendMessage, this.onAddAttachment, this.focusNode});

  @override
  _MessageInputState createState() => _MessageInputState();
}

class _MessageInputState extends State<MessageInput> {
  final textEditingController = new TextEditingController();

  bool _isMessageEntered = false;

  @override
  Widget build(BuildContext context) {
    var theme = ThemedChatContainer.of(context).theme;
    return Container(
      padding: const EdgeInsets.all(6.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // Button send image
          Material(
            child: Container(
              child: Transform.rotate(
                angle: 135 * pi / 180,
                child: IconButton(
                  icon: const Icon(Icons.attachment_rounded),
                  tooltip: 'Add attachment',
                  color: theme.primaryColor,
                  onPressed: () {
                    widget.onAddAttachment();
                  },
                ),
              ),
            ),
            color: theme.inputFieldBackgroundColor,
          ),
          // Edit text
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: 16),
              child: TextField(
                style: TextStyle(color: theme.primaryColor, fontSize: 15.0),
                controller: textEditingController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Type your message...',
                  hintStyle: TextStyle(color: theme.hintColor),
                ),
                focusNode: widget.focusNode,
                onSubmitted: (text) {
                  widget.onSendMessage(context, text, 0);
                  textEditingController.clear();
                  setState(() {
                    _isMessageEntered = false;
                  });
                },
                onChanged: (text) {
                  setState(() {
                    _isMessageEntered = text.trim().isNotEmpty;
                  });
                },
              ),
            ),
          ),
          // Button send message
          Material(
            child: Container(
              child: IconButton(
                icon: const Icon(Icons.send),
                tooltip: 'Send message',
                color: _isMessageEntered
                    ? theme.primaryColor
                    : theme.primaryColorDisabled,
                onPressed: () {
                  final text = textEditingController.text;
                  textEditingController.clear();
                  setState(() {
                    _isMessageEntered = false;
                  });
                  widget.onSendMessage(context, text, 0);
                },
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      decoration: BoxDecoration(
          border: Border(
            top: BorderSide(color: theme.lineColor, width: 1.0),
          ),
          color: Colors.white),
    );
  }

  @override
  void dispose() {
    super.dispose();
    textEditingController.dispose();
  }
}
