

import 'package:flutter/widgets.dart';

import '../model/chat_info.dart';
import '../model/message.dart';

@immutable
class ChatState {
  final bool _isCustomer;

  ChatInfo chatInfo;

  bool get isCustomer => _isCustomer;

  ChatState(this._isCustomer, {this.chatInfo});
}

class InitialChatState extends ChatState {
  InitialChatState(bool isCustomer) : super(isCustomer);
}

class ChatLoaded extends ChatState with MessagesHolder {
  final List<Message> messages;

  ChatLoaded(this.messages, bool isCustomer, ChatInfo chatInfo)
      : super(isCustomer, chatInfo: chatInfo);
}

class NewMessageState extends ChatState with MessagesHolder {
  final List<Message> messages;
  final Message newMessage;

  NewMessageState(this.messages, this.newMessage, bool isCustomer,
      ChatInfo chatInfo)
      : super(isCustomer, chatInfo: chatInfo);
}

class NewMessagesState extends ChatState with MessagesHolder {
  final List<Message> messages;

  NewMessagesState(this.messages, bool isCustomer,
      ChatInfo chatInfo)
      : super(isCustomer, chatInfo: chatInfo);
}

class ImageLoading extends ChatState with MessagesHolder {
  final List<Message> messages;

  ImageLoading(this.messages, bool isCustomer, ChatInfo chatInfo)
      : super(isCustomer, chatInfo: chatInfo);
}

mixin MessagesHolder {
  List<Message> get messages;
}
