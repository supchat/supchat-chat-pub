

import 'package:flutter/widgets.dart';

import '../model/chat_info.dart';
import '../model/message.dart';

@immutable
abstract class ChatEvent {}

class ChatOpened extends ChatEvent {
  ChatOpened();
}

class NewMessage extends ChatEvent {
  final Message message;

  NewMessage(this.message);
}

class NewMessages extends ChatEvent {
  final List<Message> messages;

  NewMessages(this.messages);
}

class OnChatHistory extends ChatEvent {
  final List<Message> history;

  OnChatHistory(this.history);
}

class OnChatInfo extends ChatEvent {
  final ChatInfo chatInfo;

  OnChatInfo(this.chatInfo);
}

class SendMessageEvent extends ChatEvent {
  final Message message;

  SendMessageEvent(this.message);
}

class SendImage extends ChatEvent {
  final dynamic data;

  SendImage(this.data);

}

