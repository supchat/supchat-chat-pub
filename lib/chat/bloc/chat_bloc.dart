import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../chat_config.dart';
import '../data/chat_repository.dart';
import '../data/user_repository.dart';
import '../model/message.dart';
import 'bloc.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  final ChatRepository _chatRepository;
  final UserRepository _userRepository;
  final ChatConfig _config;
  final String _chatId;

  List<StreamSubscription> _subscriptions = [];

  ChatBloc(
      this._chatId, this._chatRepository, this._userRepository, this._config)
      : super(InitialChatState(_config.isCustomerApp));

  @override
  Stream<ChatState> mapEventToState(
    ChatEvent event,
  ) async* {
    if (event is ChatOpened) {
      yield* _mapChatOpenedToState(event);
    } else if (event is NewMessage) {
      yield* _mapNewMessageToState(event);
    } else if (event is NewMessages) {
      yield* _mapNewMessagesToState(event);
    } else if (event is OnChatHistory) {
      yield* _mapChatHistoryToState(event);
    } else if (event is SendMessageEvent) {
      yield* _mapNewMessageSendToState(event);
    } else if (event is SendImage) {
      yield* _mapNewImageSendToState(event);
    } else if (event is OnChatInfo) {
      yield* _mapChatInfoToState(event);
    }
  }

  Stream<ChatState> _mapChatOpenedToState(ChatOpened event) async* {
    final user = await _userRepository.getChatUser();
    final userId = user.id;
    _subscriptions.forEach((element) => element.cancel());
    _subscriptions.clear();

    await _loadHistory();
    //subscribe only after history loaded
    _subscriptions
        .add(_chatRepository.onNewMessages(_chatId).listen((messages) {
      print('message receive listen ${messages.length}');
      if (messages.length == 1) {
        add(NewMessage(messages.first));
      } else {
        add(NewMessages(messages));
      }
    }, onError: (e) {
      print('message receive onError ${e}');
    }));

    await markChatSeen();

    _loadChatInfo();
  }

  Future _loadHistory() async {
    try {
      final chatHistory = await _chatRepository.getChatHistory(_chatId);
      print('chat history ${chatHistory}');
      add(OnChatHistory(chatHistory));
    } catch (e) {
      print('error ${e}');
      add(OnChatHistory([]));
    }
  }

  _loadChatInfo() {
    final subscription =
        _chatRepository.loadChatInfo(_chatId).listen((chatInfo) {
      print('chat info ${chatInfo}');
      add(OnChatInfo(chatInfo));
    });
    _subscriptions.add(subscription);
  }

  Future<dynamic> markChatSeen() async {
    final user = await _userRepository.getChatUser();
    final userId = user.id;
    return _chatRepository.markChatSeen(_chatId).catchError((error) {
      print('markChatSeen error $error');
    });
  }

  Stream<ChatState> _mapNewMessageToState(NewMessage event) async* {
    print('NewMessage');
    if (state is MessagesHolder) {
      final newMessageList =
          (state as MessagesHolder).messages.addNewMessage(event.message);
      yield NewMessageState(
          newMessageList, event.message, _config.isCustomerApp, state.chatInfo);
    } else if (state is InitialChatState) {
      yield ChatLoaded(
          List()..add(event.message), _config.isCustomerApp, state.chatInfo);
    }
    markChatSeen();
  }

  Stream<ChatState> _mapNewMessagesToState(NewMessages event) async* {
    print('NewMessages');
    if (state is MessagesHolder) {
      final newMessageList = (state as MessagesHolder).messages
        ..addAll(event.messages);
      yield NewMessagesState(
          newMessageList, _config.isCustomerApp, state.chatInfo);
    } else if (state is InitialChatState) {
      yield ChatLoaded(event.messages, _config.isCustomerApp, state.chatInfo);
    }
    markChatSeen();
  }

  Stream<ChatState> _mapChatHistoryToState(OnChatHistory event) async* {
    print('OnChatHistory $state ${event.history.length}');
    if (state is ChatLoaded) {
      final newMessageList = (state as ChatLoaded)
          .messages
          .map((message) => message)
          .toList()
            ..addAll(event.history);
      yield ChatLoaded(newMessageList, _config.isCustomerApp, state.chatInfo);
    } else if (state is InitialChatState) {
      yield ChatLoaded(
          List()..addAll(event.history), _config.isCustomerApp, state.chatInfo);
    }
  }

  Stream<ChatState> _mapChatInfoToState(OnChatInfo event) async* {
    print('state ${state}');
    state.chatInfo = event.chatInfo;
    final messages =
        (state is MessagesHolder) ? (state as MessagesHolder).messages : [];
    final newState = ChatLoaded(messages, state.isCustomer, state.chatInfo);
    yield newState;
  }

  Stream<ChatState> _mapNewMessageSendToState(SendMessageEvent event) async* {
    print('message send');
    try {
      _chatRepository.sendMessage(_chatId, event.message);
    } catch (e) {
      print('message error');
      print(e);
    }
    yield state;
  }

  Stream<ChatState> _mapNewImageSendToState(SendImage event) async* {
    if (state is! MessagesHolder) return;
    yield ImageLoading((state as MessagesHolder).messages,
        _config.isCustomerApp, state.chatInfo);
    final imageUri = await _chatRepository.uploadFile(event.data);
    yield ChatLoaded((state as ImageLoading).messages, _config.isCustomerApp,
        state.chatInfo);
    final user = await _userRepository.getChatUser();
    add(SendMessageEvent(Message(
        idFrom: user.id,
        time: DateTime.now(),
        content: imageUri,
        type: 1,
        isLast: true,
        isMy: true)));
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    print(error);
  }

  @override
  Future<void> close() {
    _subscriptions.forEach((element) => element.cancel());
    _subscriptions.clear();
    _chatRepository.close();
    return super.close();
  }

  bool get isCustomer => state.isCustomer;
}

extension on List<Message> {
  List<Message> addNewMessage(Message message) {
    return map((message) => message.copyWith(isLast: false)).toList()
      ..insert(0, message);
  }
}
