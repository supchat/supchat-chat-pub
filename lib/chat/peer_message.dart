import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:supchat_chat/chat/themed_chat_container.dart';

import '../utils/extentions.dart';
import 'message_text.dart';
import 'model/message.dart';

class PeerMessage extends StatelessWidget {
  final Message message;
  final Function onImageClick;

  PeerMessage({this.message, this.onImageClick});

  @override
  Widget build(BuildContext context) {
    var theme = ThemedChatContainer.of(context).theme;
    return Container(
      padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
      child: Column(
        children: <Widget>[
          message.type == 0
              ? Container(
                  constraints: BoxConstraints(maxWidth: 240),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        child: MessageText(
                          text: message.content,
                          color: theme.messagePeerTextColor,
                        ),
                      ),
                      const SizedBox(width: 24.0),
                      Text(
                        message.time.formatTime(),
                        overflow: TextOverflow.clip,
                        style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                color: Color(0xff55658A), fontSize: 10)),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 10.0),
                  decoration: BoxDecoration(
                      color: theme.messagePeerBackgroundColor,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(5.0),
                          topRight: Radius.circular(20.0),
                          bottomLeft: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0))),
                )
              : Container(
                  child: FlatButton(
                    child: Material(
                      child: Stack(
                        children: [
                          Image(
                            image: NetworkImage(message.content),
                            width: 200.0,
                            height: 200.0,
                            fit: BoxFit.cover,
                          ),
                          Container(
                            width: 200.0,
                            height: 30.0,
                            decoration: const BoxDecoration(
                              gradient: const LinearGradient(
                                  colors: [
                                    const Color(0x55000000),
                                    const Color(0x00000000),
                                  ],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.0, 1.0),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 16.0, top: 10.0),
                              child: Text(
                                message.time.formatTime(),
                                textAlign: TextAlign.end,
                                style: GoogleFonts.montserrat(
                                    textStyle: const TextStyle(
                                        color: Colors.white, fontSize: 10)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(4.0),
                          topRight: Radius.circular(20.0),
                          bottomLeft: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0)),
                      clipBehavior: Clip.antiAlias,
                    ),
                    onPressed: () {
                      onImageClick(message.content);
                    },
                    padding: const EdgeInsets.all(0),
                  ),
                  margin: const EdgeInsets.only(left: 10.0),
                ),
        ],
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }
}
