import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_fonts/google_fonts.dart';

class MessageText extends StatelessWidget {
  final String text;
  final Color color;

  const MessageText({Key key, this.text, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        HapticFeedback.vibrate();
        Clipboard.setData(ClipboardData(text: text));
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            duration: Duration(seconds: 1),
            content: const Text('Copied to clipboard'),
          ),
        );
      },
      child: Linkify(
        onOpen: (link) async {
          if (await canLaunch(link.url)) {
            await launch(link.url);
          }
        },
        text: text,
        options: LinkifyOptions(humanize: false),
        style: GoogleFonts.montserrat(
            textStyle: TextStyle(color: color, fontSize: 12)),
        linkStyle: GoogleFonts.montserrat(
            textStyle: TextStyle(color: color, fontSize: 12)),
      ),
    );
  }
}
