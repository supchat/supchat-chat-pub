import 'package:intl/intl.dart';

extension datetime on DateTime {
  String formatTime() {
    final dateFormat = DateFormat('HH:MM');
    return dateFormat.format(this);
  }

  bool isToday() {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final date = DateTime(this.year, this.month, this.day);
    return date == today;
  }

  bool isYesterday() {
    final now = DateTime.now();
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    final date = DateTime(this.year, this.month, this.day);
    return date == yesterday;
  }
}
