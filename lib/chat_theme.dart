import 'dart:ui';

class ChatTheme {
  final Color primaryColor;
  final Color primaryColorDisabled;
  final Color backgroundColor;
  final Color backgroundDarkColor;
  final Color textColor;
  final Color topBarColor;
  final Color messageOwnTextColor;
  final Color messageOwnBackgroundColor;
  final Color messagePeerTextColor;
  final Color messagePeerBackgroundColor;
  final Color inputFieldBackgroundColor;
  final Color hintColor;
  final Color lineColor;
  final Color dateDividerTextColor;
  final double horizontalPadding;
  final double verticalPadding;

  const ChatTheme({
    this.primaryColor = const Color(0xff0F2253),
    this.primaryColorDisabled = const Color(0x5555658A),
    this.backgroundColor = const Color(0xffF3F8FF),
    this.backgroundDarkColor = const Color(0xff000014),
    this.textColor = const Color(0xff0F2253),
    this.topBarColor = const Color(0xff8E91F4),
    this.messageOwnTextColor = const Color(0xffFFFFFF),
    this.messageOwnBackgroundColor = const Color(0xff6CA0FF),
    this.messagePeerTextColor = const Color(0xff55658A),
    this.messagePeerBackgroundColor = const Color(0xffFFFFFF),
    this.inputFieldBackgroundColor = const Color(0xffFFFFFF),
    this.hintColor = const Color(0xffAEAEAE),
    this.lineColor = const Color(0x3355658A),
    this.dateDividerTextColor = const Color(0xff55658A),
    this.horizontalPadding = 12.0,
    this.verticalPadding = 18.0,
  });
}
